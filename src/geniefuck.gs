/* Brainfuck interpreter in Genie 
 * 
 * Reads a program from the stdin/filename (first argument) and interprets it.
 * 
 * Copyright (c) 2010 Quildreen <quildreen@gmail.com>
 * Licenced under MIT licence.
 */

/* Tokens we can have on the code */
enum token
	NONE
	NEXT_CELL
	PREV_CELL
	INC
	DEC
	OUT
	IN
	LOOP_START
	LOOP_END


/* max number of memory positions on the tape */
const MAX_TAPE: int = 30000


/**
 * Holds information about each Node on the AST
 */
class ASTNode: Object
	token_: token        // token of this item
	times:  int          // number of times to repeat this item
	prev:   weak ASTNode // reference to previous node
	next:   ASTNode      // reference to next node
	link:   ASTNode      // special link to this node (used to speed up loops)
	
	construct (tk: token, repeat: int, prev_: ASTNode? = null
	          ,link_: ASTNode? = null)
		token_ = tk
		times  = repeat
		prev   = prev_
		link   = link_
		if link_ is not null do link_.link = self
		if prev_ is not null do prev_.next = self
		
		
/**
 * Implements a tree of AST nodes
 */
class AST: Object
	root: ASTNode
	cur:  ASTNode
	
	/**
	 * Creates a new node and adds it to the tree
	 */
	def add(token_: token, repeat: int, link: ASTNode? = null): ASTNode
		cur = new ASTNode(token_, repeat, cur, link)
		if root is null do root = cur
		return cur
	
	/**
	 * Adds an existing node to the tree
	 */
	def add_node(node: ASTNode): ASTNode
		return add(node.token_, node.times, node.link)

/**
 * Parses brainfuck code and generates an AST from that.
 */
class Parser: Object
	/**
	 * Maps tokens to their enums
	 */
	def inline get_token(c: int): token
		var rv = token.NONE
		
		case c
			when '>' do rv = token.NEXT_CELL
			when '<' do rv = token.PREV_CELL
			when '+' do rv = token.INC
			when '-' do rv = token.DEC
			when '.' do rv = token.OUT
			when ',' do rv = token.IN
			when '[' do rv = token.LOOP_START
			when ']' do rv = token.LOOP_END
			
		return rv
	
	def inline add(ast: AST, ref node: ASTNode, ref link: ASTNode?)
		node = ast.add_node(node)
		if node.token_ == token.LOOP_START
			link = node
		if node.token_ == token.LOOP_END
			assert link is not null
			node.link = link
			link = null
	
	/**
	 * Does the actual parsing
	 */
	def parse(data:FileStream, out ast:AST)
		var tk    = token.NONE // current token
		var c     = data.EOF   // current read character
		var node  = new ASTNode(tk, 1)
		link:ASTNode = null
		
		ast = new AST
		while (c = data.getc()) != data.EOF
			if (tk = get_token(c)) != token.NONE
				if tk == node.token_ \
				   and (tk >= token.NEXT_CELL and tk <= token.DEC)
					node.times++
				else
					add(ast, ref node, ref link)
					node = new ASTNode(tk, 1)
				node.token_ = tk
		add(ast, ref node, ref link)


/**
 * Executes a brainfuck AST
 */
class Interpreter: Object
	pos:  int
	node: ASTNode
	tape: array of char
	ast:  AST
	
	/**
	 * Constructs from a AST list
	 */
	construct (data: AST)
		pos  = 0
		ast  = data
		tape = new array of char[MAX_TAPE]
		
	/**
	 * Constructs from a file object
	 */
	construct from_file(fstream: FileStream)
		pos   = 0
		tape  = new array of char[MAX_TAPE]
		
		parser:Parser = new Parser
		parser.parse(fstream, out ast)
	 
	/**
	 * Moves the pointer
	 */
	def inline move(amount: int)
		pos = (pos + amount) % MAX_TAPE
	
	/**
	 * Adds a given amount to the cell at the current pointer
	 */
	def inline add(amount: int)
		tape[pos] = (char)((tape[pos] + amount) % 256)
		
	/**
	 * Outputs the ASCII character at the current pointer
	 */
	def inline output()
		stdout.putc(tape[pos])
		
	/**
	 * Inputs an ASCII character in the current pointer
	 */
	def inline input()
		tape[pos] = (char)stdin.getc()
		
	/**
	 * Starts a loop
	 */
	def inline loop()
		if tape[pos] == 0 and node.link is not null
			node = node.link
			
	/**
	 * Ends a loop
	 */
	def inline end_loop()
		if tape[pos] > 0 and node.link is not null
			node = node.link
	
	/**
	 * Run the AST
	 * 
	 * Returns exit code
	 */
	def run()
		node = ast.root
		while node is not null
			case node.token_
				when token.NEXT_CELL do  move(node.times)
				when token.PREV_CELL do  move(-node.times)
				when token.INC do        add(node.times)
				when token.DEC do        add(-node.times)
				when token.OUT do        output()
				when token.IN do         input()
				when token.LOOP_START do loop()
				when token.LOOP_END do   end_loop()
			
			node = node.next
			
		stdout.putc('\n')


// --[ MAIN ]-------------------------------------------------------------------
init
	i:Interpreter
	if args[1] is not null
		i = new Interpreter.from_file(FileStream.open(args[1], "r"))
	else
		i = new Interpreter.from_file(stdin)
		
	i.run()
