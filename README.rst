Geniefuck
=========

Geniefuck is a brainfuck interpreter written in `Genie`_, a pretty cute new
language with Python/Boo/Delphi influences and that compiles to C.

To get shit up and running you'll either need a Vala/Genie compiler or an C
compiler (since Geniefuck ships with both sources). You'll also need
libgee-dev1.0. Genie sources are located on ``src`` folder, while C sources are
on ``srcc`` folder.

To compile from the Genie sources::

   $ valac --pkg=gee-1.0 -o bin/geniefuck src/geniefuck.gs 

To compile from the C sources::

   $ gcc -o bin/geniefuck srcc/geniefuck.c `pkg-config --libs --cflags \
   gobject-2.0 gee-1.0`

To test shit out::

   $ bin/geniefuck test/hello.bf


.. _Genie: http://live.gnome.org/Genie

Licence
-------

MIT/X11 licenced. 'Nuff said :3
